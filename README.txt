Description
-----------

This module provides a page listing nodes by title, grouping them alphabetically, and caching the page for increased performance. The list is accessible at http://www.example.com/node/list.

In addition, this module will list nodes by title per vocabulary and per taxonomy term. These lists are available at http://www.example.com/taxonomy/term/1/list and http://www.example.com/vocabulary/1/list, where the number 1 is replaced with the term ID or vocabulary ID.

Installation
------------

1) Copy nodelist.module to your modules/ directory.

2) Visit the "admin/build/modules" page to enable the module.

3) Visit "admin/settings/nodelist" to configure the module.

4) View the list of nodes.  All nodes are listed at "node/list",
   nodes per vocabulary are listed at "vocabulary/1/list", and
   per taxonomy node lists are accessible at "taxonomy/term/1/list".

Known issues
------------

Symptom: International Unicode characters are treated like punctuation and ignored, resulting in nodes which are listed under the incorrect heading.

Problem: This module uses PHP's strtolower() and preg_replace() functions during the process of sorting nodes into an alphabetical list.  In typical PHP installations, these functions do not treat international characters as letters.

Solution: Recompile PHP with the non-standard Multibyte String Functions enabled, using both the --enable-mbstring=all and the --enable-mbregex configuration options.  (See http://www.php.net/manual/en/ref.mbstring.php for more details.) Then replace all occurances of strtolower() with mb_strtolower().

Followup: For more discussion about Unicode and Multibyte String Functions as they relate to this module, see http://drupal.org/node/21638.

Module authors
--------------

Nic Ivy (http://drupal.org/user/6194)
Wim Mostrey (http://drupal.org/user/21228)
